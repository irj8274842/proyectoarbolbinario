class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None


class ArbolBinario:
    def __init__(self):
        self.raiz = None

    def insertar(self, elemento):
        if self.raiz == None:
            self.raiz = Nodo(elemento)
        else:
            self.auxiliarIns(self.raiz, elemento)
         
    def auxiliarIns(self, nodo, valor):
        if valor <= nodo.valor:
            #Se crea un nodo si es que no hay
            if nodo.izquierda == None:
                nodo.izquierda = Nodo(valor)
            #Se llama de nuevo la función
            else:
                self.auxiliarIns(nodo.izquierda, valor)
        else:
            if nodo.derecha == None:
                nodo.derecha = Nodo(valor)
            else:
                self.auxiliarIns(nodo.derecha, valor)
           
        pass
       
    def eliminar(self, elemento):
        pass

    def existe(self, elemento):
        return self.auxiliarExs(self.raiz, elemento)
        pass
       
    def auxiliarExs(self, nodo, valor):
        if nodo == None:
            return False
        else:
            if valor == nodo.valor:
                return True
            elif valor < nodo.valor:
                return self.auxiliarExs(nodo.izquierda, valor)
            else:
                return self.auxiliarExs(nodo.derecha, valor)

    def maximo(self):
        if self.raiz == None:
            return None
        else:
            n = self.raiz
            while n.derecha != None:
                n =  n.derecha
            return n.valor

    def minimo(self):
        if self.raiz == None:
            return None
        else:
            n = self.raiz
            while n.izquierda != None:
                n = n.izquierda
            return n.valor

    def altura(self):
        return self.auxiliarAlt(self.raiz)
       
    def auxiliarAlt(self, nodo):
        if nodo == None:
            return 0
        else:
            return max(self.auxiliarAlt(nodo.izquierda), self.auxiliarAlt(nodo.derecha)) +1
   
    def obtener_elementos(self):
        return self.obtener_elementos_aux(self.raiz, [])
   
    def obtener_elementos_aux(self, n, arr):
        if n.izquierda != None:
            self.obtener_elementos_aux(n.izquierda, arr)
        arr.append(n.valor)
        if n.derecha != None:
            self.obtener_elementos_aux(n.derecha, arr)
        return arr
           

    def _str_(self):
        return "[ " + " ".join(map(str, self.obtener_elementos())) + " ]"
        