## Indicaciones

- Haz un fork del [repositorio Practica06](https://gitlab.com/estructurasdatos1/practica06)
- Agrega a los instructores de laboratorio como mantenedores: [Edgar](https://gitlab.com/leonKj), [David](https://gitlab.com/davidalencia)
- Marca como completada en el classroom y como comentario agrega el link a tu repositorio.

## Probar tu codigo

- Instala pytest

```bash
pip install pytest
```

Finalmente en la raiz de tu directorio ejecuta el siguiente comando:

```bash
python -m pytest tests
```
